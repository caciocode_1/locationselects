<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Country;
use AppBundle\Entity\City;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AppBundle\Entity\User;
use Symfony\Component\Intl\Intl;

class LoadCitiesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $i=1;
        $cities = [
            ["name"=>"Portsmouth", "region"=>"R_EE"],
            ["name"=>"Brighton", "region"=>"R_EE"],
            ["name"=>"Southampton", "region"=>"R_EE"],
            ["name"=>"Hastings", "region"=>"R_EE"],
            ["name"=>"Oxford", "region"=>"R_EE"],
            ["name"=>"Eastbourne", "region"=>"R_EE"],
            ["name"=>"Reading", "region"=>"R_EE"],
            ["name"=>"Chichester", "region"=>"R_EE"],
            ["name"=>"Brighton and Hove", "region"=>"R_EE"],
            ["name"=>"Windsor", "region"=>"R_EE"],
            ["name"=>"City of Canterbury", "region"=>"R_EE"],
            ["name"=>"Dover", "region"=>"R_EE"],
            ["name"=>"Worthing", "region"=>"R_EE"],
            ["name"=>"Crawley", "region"=>"R_EE"],
            ["name"=>"Slough", "region"=>"R_EE"],


            ["name"=>"Nottingham", "region"=>"R_EM"],
            ["name"=>"Leicester", "region"=>"R_EM"],
            ["name"=>"Derby", "region"=>"R_EM"],
            ["name"=>"Loughborough", "region"=>"R_EM"],
            ["name"=>"Northampton", "region"=>"R_EM"],
            ["name"=>"Chesterfield", "region"=>"R_EM"],
            ["name"=>"Chesterfield", "Castle Donington", "region"=>"R_EM"],
            ["name"=>"Chesterfield", "Mansfield", "region"=>"R_EM"],
            ["name"=>"Chesterfield", "Corby", "region"=>"R_EM"],
            ["name"=>"Chesterfield", "Long Eaton", "region"=>"R_EM"],
            ["name"=>"Chesterfield", "Skegness", "region"=>"R_EM"],
            ["name"=>"Chesterfield", "Wellingborough", "region"=>"R_EM"],
            ["name"=>"Chesterfield", "Bakewell", "region"=>"R_EM"],
            ["name"=>"Chesterfield", "Brough of Melton", "region"=>"R_EM"],
            ["name"=>"Slough", "region"=>"R_EM"]
        ];
        foreach ($cities as $city){
            $new_city = new City();
            $new_city->setName($city['name']);
            if ($this->hasReference('GB')){
                $country = $this->getReference('GB');
                $country->addCity($new_city);
                $manager->persist($country);
                $manager->flush();
            }
            if ($this->hasReference($city['region'])){
                $region = $this->getReference($city['region']);
                $region->addCity($new_city);
                $manager->persist($region);
                $manager->flush();
            }
        }
        /*while (file_exists(__DIR__.'/data/world_cities_array/world_cities_array_'.$i.'.php')){
            require (__DIR__.'/data/world_cities_array/world_cities_array_'.$i.'.php');
            //print_r ($cities_array);
            $z=0;
            foreach ($cities_array as $city){
                if ($z>300){ break; }
                $new_city = new City();
                $new_city->setName($city['city']);
                //$city['country'];
                //$manager->persist($new_city);
                //$manager->flush();
                if ($this->hasReference($city['country'])){
                    $country = $this->getReference($city['country']);
                    $country->addCity($new_city);
                    $manager->persist($country);
                    $manager->flush();
                }
                $z++;
            }
            $i++;

        }*/
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 3;
    }
}
?>