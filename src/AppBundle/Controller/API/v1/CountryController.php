<?php
// src/AppBundle/Controller/CountryController.php

namespace AppBundle\Controller\API\v1;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CountryController extends FOSRestController
{
    /**
     * @Rest\Get("/countries")
     */
    public function getCountriesAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Country');
        $countries = $repository->findAll();
        //$view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        //return $view;
        return $countries;
    }

    /**
     * @Rest\Get("/countries/{code_or_id}")
     */
    public function getCountryAction($code_or_id, Request $request)
    {
        if (is_numeric($code_or_id)){
            $country = $this->getDoctrine()->getRepository('AppBundle:Country')->find($code_or_id);
        }else {
            $country = $this->getDoctrine()->getRepository('AppBundle:Country')->findBy(array(
                'code2a' =>  $code_or_id
            ))[0];
        }
        return $country;
    }

    /**
     * @Rest\Get("/countries/{code_or_id}/regions")
     */
    public function getCountryRegionsAction($code_or_id, Request $request)
    {
        if (is_numeric($code_or_id)){
            $country = $this->getDoctrine()->getRepository('AppBundle:Country')->find($code_or_id);
            $regions = $country->getRegions();
        }else {
            $country = $this->getDoctrine()->getRepository('AppBundle:Country')->findBy(array(
                'code2a' =>  $code_or_id
            ))[0];
            $regions = $country->getRegions();
        }
        return $regions;
    }

    /**
     * @Rest\Post("/countries")
     */
    public function postCountriesAction(Request $request)
    {
        $data = ['postCountriesAction' => 'not implemented yet'];
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        return $view;
    }

    /**
     * @Rest\Put("/countries/{userId}")
     */
    public function putCountriesByIdAction(Request $request)
    {
        $userId = $request->get('userId');
        $data = ['putCountriesByIdAction' => 'not implemented yet'];
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        return $view;
    }

    /**
     * @Rest\Delete("/countries/{userId}")
     */
    public function deleteCountriesByIdAction(Request $request)
    {
        $userId = $request->get('userId');
        $data = ['deleteCountriesByIdAction' => 'not implemented yet'];
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        return $view;
    }

}

?>