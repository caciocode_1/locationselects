<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="country_id", type="integer")
     */
    private $countryId;

    /**
     * @var int
     *
     * @ORM\Column(name="region_id", type="integer", nullable=true)
     */
    private $regionId;

    /**
     * @var int
     *
     * @ORM\Column(name="city_id", type="integer", nullable=true)
     */
    private $cityId;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="Customers", fetch="EAGER")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    public $country;

    /**
     * @ORM\ManyToOne(targetEntity="City", fetch="EAGER")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    public $city;

    /**
     * @ORM\ManyToOne(targetEntity="Region", fetch="EAGER")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $Region;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set countryId
     *
     * @param integer $countryId
     *
     * @return Customer
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return int
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set regionId
     *
     * @param integer $regionId
     *
     * @return Customer
     */
    public function setRegionId($regionId)
    {
        $this->regionId = $regionId;

        return $this;
    }

    /**
     * Get regionId
     *
     * @return int
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * Set cityId
     *
     * @param integer $cityId
     *
     * @return Customer
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;

        return $this;
    }

    /**
     * Get cityId
     *
     * @return int
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Add Country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get Country
     *
     * @return $this
     */
    public function setCountry (Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get City
     *
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add Country
     *
     * @return $this
     */
    public function setCity (City $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get Region
     *
     * @return Region
     */
    public function GetRegion()
    {
        return $this->Region;
    }

    /**
     * Add Region
     *
     * @return $this
     */
    public function setRegion (Region $region)
    {
        $this->Region = $region;
        return $this;
    }
}

